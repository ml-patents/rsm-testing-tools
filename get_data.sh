#!/bin/bash

if [ "$#" -eq  "0" ]
then
    python parser.py lyrl2004_tokens_test_pt0.dat dictionary.txt > test0.txt
    python parser.py lyrl2004_tokens_test_pt1.dat dictionary.txt > test1.txt
    python parser.py lyrl2004_tokens_test_pt2.dat dictionary.txt > test2.txt
    python parser.py lyrl2004_tokens_test_pt3.dat dictionary.txt > test3.txt
    python parser.py lyrl2004_tokens_train.dat dictionary.txt > train.txt
else
    python parser.py lyrl2004_tokens_test_pt0.dat dictionary.txt > $1
    python parser.py lyrl2004_tokens_test_pt1.dat dictionary.txt >> $1
    python parser.py lyrl2004_tokens_test_pt2.dat dictionary.txt >> $1
    python parser.py lyrl2004_tokens_test_pt3.dat dictionary.txt >> $1
    python parser.py lyrl2004_tokens_train.dat dictionary.txt >> $1
fi