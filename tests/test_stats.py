# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os
import unittest
import numpy as np
import stats


class TestFMatrix(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_vectors_length(self):
        matrix = np.zeros((3, 5))
        matrix[1, 3] = 4
        vector = stats.get_vectors_length(matrix)
        self.assertEqual(vector.size, 3)
        self.assertTrue(vector[1] == 4)

    def test_cosine_distance(self):
        a = np.array([1, 2, 3])
        b = np.array([3, 4, 5])
        self.assertEqual(stats.cosine_distance(a, b), 0.98270762982399085)
        self.assertEqual((stats.cosine_distance(a, b, np.linalg.norm(b))), stats.cosine_distance(a, b))

    def test_draw_chart(self):
        filename = 'test_chart123.png'
        if os.path.exists(filename):
            os.remove(filename)
        self.assertFalse(os.path.exists(filename))

        stats.draw_chart([0.0, 0.25, 0.5, 0.75, 1], [0.9, 0.8, 0.5, 0.1, 0.0], filename)
        self.assertTrue(os.path.exists(filename))
        os.remove(filename)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
