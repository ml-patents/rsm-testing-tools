# -*- coding: utf-8 -*-
#!/usr/bin/env python

import unittest
import numpy as np
import pickle
import os
from stats import main


class TestStatsAcceptanceTest(unittest.TestCase):
    model_filename = 'model_random_test'

    def setUp(self):

        model = {'w_vh': np.random.uniform(0, 1, size=(10000, 500)),
                 'w_v': np.random.uniform(0, 1, 10000),
                 'w_h': np.random.uniform(0, 1, 500)}
        with open(self.model_filename, 'wb') as filename:
            pickle.dump(model, filename, 1)

    def test_stats_with_random_neural_network(self):
        main('rcv1-v2.topics.qrels', self.model_filename, 'test', 'dictionary.txt', 'test_output.png')

    def tearDown(self):
        if os.path.isfile(self.model_filename):
            os.remove(self.model_filename)


if __name__ == '__main__':
    unittest.main()
