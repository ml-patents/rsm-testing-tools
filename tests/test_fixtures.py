# -*- coding: utf-8 -*-
#!/usr/bin/env python

import sys
sys.path.append('../')
import unittest
import stats


class TestFixtures(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_categories_dictionary(self):
        categories = stats.get_categories_dictionary('test_categories.txt')
        self.assertTrue(len(categories.get(4)), 2)

    def test_belongs_to_same_category(self):
        categories = stats.get_categories_dictionary('test_categories.txt')
        self.assertTrue(stats.belongs_to_same_category(categories, 1, 3))

    def test_belongs_to_same_category_negative_test(self):
        categories = stats.get_categories_dictionary('test_categories.txt')
        self.assertFalse(stats.belongs_to_same_category(categories, 1, 'a'))

    def test_get_precission_and_recall(self):
        list = [True, True, False, True, False, True, False, True]
        print stats.get_precision_and_recall(list, 4)
        self.assertTrue(stats.get_precision_and_recall(list, 4))

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
